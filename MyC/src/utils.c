#include <stdio.h>
#include <stdlib.h>

#include "Table_des_symboles.h"
#include "Attribute.h"
#include "utils.h"



int new_label()
{
  static int label = 0; 
  return label++;
}

int new_register()
{
  static int i = 0;

  return i++;
}

char type_register(attribute a)
{
  char c;

  if (a->type_val == INT)
    c = 'i';
  if (a->type_val == FLOAT)
    c = 'f';
  return c;
}

//create code operations

void code_operation(attribute res, attribute left, attribute right, char op)  //with op = +,-,*,/
{
  int reg;
  int reg_left = left->reg_number;
  int reg_right = right->reg_number;

  //on effectue d'abord la cast si besoin  

  if (left->type_val == INT && right->type_val == FLOAT) {
    reg = new_register();
    fprintf(result_c, "  rf%d = (float) ri%d;\n", reg, left->reg_number); 
    fprintf(result_header, "float rf%d;\n", reg);
    reg_left = reg;
    res->type_val = FLOAT;
  }
  else if (left->type_val == FLOAT && right->type_val == INT) {
    reg = new_register();
    fprintf(result_c, "  rf%d = (float) ri%d;\n", reg, right->reg_number);
    fprintf(result_header, "float rf%d;\n", reg);
    reg_right = reg;
    res->type_val = FLOAT;
  }
  else if (left->type_val == FLOAT && right->type_val == FLOAT){
    res->type_val = FLOAT;
  }
  else if (left->type_val == INT && right->type_val == INT){
    res->type_val = INT;
  }

  // puis productionde code arithmetique
  
  reg = new_register();
  if (res->type_val == INT) {
    fprintf(result_c, "  ri%d = ri%d %c ri%d;\n", reg, reg_left, op, reg_right); 
    fprintf(result_header, "int ri%d;\n", reg);
    res->type_val = INT;
  }
  else if (res->type_val == FLOAT) {
    fprintf(result_c, "  rf%d = rf%d %c rf%d;\n", reg, reg_left, op, reg_right); 
    fprintf(result_header, "float rf%d;\n", reg);
    res->type_val = FLOAT;
  }
  res->reg_number = reg;
}


void neg_operation(attribute res, attribute src)
{
  int reg = new_register();
  
  if (src->type_val == INT)
    {
      fprintf(result_c, "  ri%d = - ri%d;\n", reg, src->reg_number);
      fprintf(result_header, "int ri%d;\n", reg);
    }

  else if (src->type_val == FLOAT)
    {
      fprintf(result_c, "  rf%d = - rf%d;\n", reg, src->reg_number);
      fprintf(result_header, "float rf%d;\n", reg);
    }
  res->reg_number = reg;
}



void code_comparison(attribute res, attribute left, attribute right, char* comp)  //with comp = &&, ||, <, >, ==, !=
{
  int reg;
  int reg_left = left->reg_number;
  int reg_right = right->reg_number;
    
  reg = new_register();
  res->reg_number = reg;
  res->type_val = INT;
  
  fprintf(result_c, "  ri%d = r%c%d %s r%c%d;\n", reg,type_register(left), reg_left, comp,type_register(right), reg_right); 
  fprintf(result_header, "int ri%d;\n", reg);
}



void neg_comparison(attribute res, attribute src)
{
  int reg;
  reg = new_register();
  res->reg_number = reg;
  res->type_val = INT;

  fprintf(result_c, "  ri%d = !r%c%d;\n", reg,type_register(src), src->reg_number); 
  fprintf(result_header, "int ri%d;\n", reg);
  
}
