#ifndef UTILS_H
#define UTILS_H

#include "Attribute.h"

static char *my_files[] = { "test.c", "test.h" };


extern FILE *result_c;
extern FILE *result_header;

int new_label();

int new_register();

char type_register(attribute a);

void code_operation(attribute res, attribute left, attribute right, char op);

void neg_operation(attribute res, attribute src);

void code_comparison(attribute res, attribute left, attribute right, char* comp);

void neg_comparison(attribute res, attribute src);

#endif
