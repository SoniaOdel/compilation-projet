%code requires{
#include "./src/Table_des_symboles.h"
#include "./src/Attribute.h"
#include "./src/utils.h"
#include "./src/Table_des_chaines.h"
  //# define YYDEBUG 1 => aide au debuggage
  //int errors; => pour compter le nombre d'erreurs du code
 }

%{

#include <stdio.h>
#include <string.h>

extern int yylex();
extern int yyparse();

FILE *result_c;
FILE *result_header;
 static int current_block = 0; 
void yyerror (char* s) {
  printf ("%s\n",s);

}


%}

%union {
	attribute val;
}
%type <val> vlist
%type <val> fun fun_body fun_head func_id params
%type <val> type
%type <val> block
%type <val> decl_list
%type <val> inst_list
%type <val> typename
%type <val> aff bool_cond if else or stat while_cond while cond
%type <val> exp app args arglist app_id
%type <val> vir did
%type <val> inst ret
%type <val> var_decl
%token <val> NUMI NUMF
%token TINT TFLOAT STRUCT
%token <val> ID
%token AO AF PO PF PV VIR
%token RETURN VOID EQ
%token <val> IF ELSE WHILE

%token <val> AND OR NOT DIFF EQUAL SUP INF
%token PLUS MOINS STAR DIV
%token DOT ARR

%left DIFF EQUAL SUP INF       // low priority on comparison
%left PLUS MOINS               // higher priority on + -
%left STAR DIV                 // higher priority on * /
%left OR                       // higher priority on ||
%left AND                      // higher priority on &&
%left DOT ARR                  // higher priority on . and ->
%nonassoc UNA                  // highest priority on unary operator

%start prog



%%

prog : block                   {}
;

block:
decl_list inst_list            {}
;

// I. Declarations

decl_list : decl decl_list     {}
|                              {}
;

decl: var_decl PV              {}

| struct_decl PV               {}
| fun_decl                     {}
;

// I.1. Variables                                    
var_decl : type vlist          {}

// I.2. Structures
struct_decl : STRUCT ID struct {}
;

struct : AO attr AF            {}
;

attr : type ID                 {}
| type ID PV attr              {}

// I.3. Functions                                 

fun_decl : type fun            {}
;

fun : fun_head fun_body        {
                                dequeue(current_block); //on sort d'un bloc, on dépile donc les variables créées dans ce bloc
				current_block -=1;     
                               }
;

fun_head : func_id PO PF        {
                                 fprintf(result_c, ")\n{\n");
                                }



| func_id  PO params PF         {
                                 fprintf(result_c,")\n{\n");
                                }
;


func_id : ID                   {
                                $1->type_val =$<val>0->type_val;
				set_symbol_value($1->name, $1, current_block); // on ajoute un id dans la table de symboles avec le numero du 
                                fprintf(result_c, "%s %s( ", $<val>0->name, $1->name);//ou il a été créé
				current_block +=1;
                               }

;


params: type ID vir params     {
                                $2->type_val = $1->type_val;
				set_symbol_value($2->name, $2, current_block);
                                fprintf(result_c,", %s %s,", $1->name, $2->name);
                               }

 
| type ID                      {
                                $2->type_val = $1->type_val;
				set_symbol_value($2->name, $2, current_block);
                                fprintf(result_c,"%s %s", $1->name, $2->name);
                               }



vlist: did vir vlist           {}

| did                          {}
;

did : ID                       {
                                $$ = $<val>0; $1->reg_number = new_register();
				$1->type_val = $$->type_val;
				set_symbol_value($1->name, $1, current_block);
                                fprintf(result_header, "%s r%c%d;\n %s %s;\n", $$->name, type_register($1), $1->reg_number, $$->name, $1->name);
                                }
;

vir : VIR                      {$$= $<val>-1;} //on recupère le type
;

fun_body : AO block AF         {fprintf(result_c,"\n}\n");}
;

// I.4. Types
type
: typename pointer             {}
| typename                     {$$=$1;}
;

typename
: TINT                         {$$ = new_attribute();  $$->type_val = INT; $$->name = "int";}
| TFLOAT                       {$$ = new_attribute();  $$->type_val = FLOAT; $$->name = "float";}
| VOID                         {$$ = new_attribute();  $$->type_val = VOID; $$->name = "void";}
| STRUCT ID                    {/*$$ = new_attribute();  $$->name  = $2->name; $$->type_val = STRUCT;*/} // trouver une solution pour struct
;

pointer
: pointer STAR                 {}
| STAR                         {}
;


// II. Instructions

inst_list: inst PV inst_list   {}
| inst                         {}
|                              {}
;

inst: //inutile de remonter de l'information
exp                           {}
| AO block AF                 {}
| aff                         {}
| ret                         {}
| cond                        {}
| loop                        {}
| PV                          {}
;


// II.1 Affectations

 aff : ID EQ exp               { attribute tmp = get_symbol_value($1->name);
                                  if(tmp->type_val != $3->type_val){
				   yyerror("WARNING! affectation d'une variable à une autre de type différent");}
				 fprintf(result_c,"  %s = r%c%d;\n",tmp->name,type_register($3),$3->reg_number);
				 if($3->type_val == INT)
				   fprintf(result_c,"  printf(\"%s = %sd\\n\",%s);\n", tmp->name,"%", tmp->name); //des printf pour verifier que l'affectation ou le calcul a été fait
				 else if($3->type_val == FLOAT)
				   fprintf(result_c,"  printf(\"%s = %sf\\n\",%s);\n", tmp->name,"%", tmp->name);
				}



| exp STAR EQ exp             {}
;


// II.2 Return
ret : RETURN exp              {fprintf(result_c,"  return r%c%i;\n", type_register($2), $2->reg_number);}

| RETURN PO PF                {fprintf(result_c,"\n  return ();\n");}
;

// II.3. Conditionelles
cond :
if bool_cond stat or stat      { //ajout d'une règle or pour faciliter la differenciation
                                fprintf(result_c,"\nl%d:;\n",$4->reg_number);
                                dequeue(current_block);
				current_block-=1;
                               }

| if bool_cond stat or         {}
;

stat:
AO block AF                   {$$ = $<val>0;}
;

bool_cond : PO exp PF         {$$ = $<val>0; //l'astuce est de tester si bool_cond est faux car le block stat du cas ou bool_cond est vrai sera affiché juste apres
                               fprintf(result_c, "  if (!r%c%d)\n     goto l%d;\n", type_register($2),$2->reg_number, $$->reg_number); 
                              }
;

or : else                     {$$ = $1;}

|                             {
                               $$=$<val>0; fprintf(result_c,"l%d:;\n", $$->reg_number); //on recupere avec $<val>0->reg_number le numero de label créé dans bool_cond
                              }
;
       

if : IF                       {current_block +=1;
                               $$ = new_attribute(); $$->reg_number = new_label();
                              }
;

else : ELSE                   {
                                $$ = $<val>0;
				int label = new_label();
                                fprintf(result_c,"  goto l%d;\nl%d:;\n",label, $$->reg_number);
				$$->reg_number = label;
                              }
;

// II.4. Iterations

loop : while while_cond inst  { //La syntaxe nous a été imposé avec une instruction dans un while et pas une liste d'instruction.
                                //Si c'etait une liste d'instruction il aurait simplement fallu ajouter des accolade dans la production de code
                                                            
			       int reg = $2->reg_number;
                               fprintf(result_c, "  goto loop_%d;\nl%d:;\n", reg-1, reg);
			       dequeue(current_block);
			       current_block -= 1;
			      }
;

while_cond : PO exp PF        {
                               $$ =new_attribute();
                               $$->reg_number = new_label();
                               fprintf(result_c, "  if (!r%c%d)\n    goto l%d;\n", type_register($2),$2->reg_number, $$->reg_number);
                               }


while : WHILE                 {
                                current_block += 1;
                                $$ = new_attribute(); $$->reg_number = new_label() ;fprintf(result_c, "loop_%d:;\n", $$->reg_number);
       			      }
;



// II.3 Expressions
exp
// II.3.0 Exp. arithmetiques

: MOINS exp %prec UNA         {neg_operation($$,$2);}


| exp PLUS exp                {code_operation($$,$1,$3,'+');}

| exp MOINS exp               {code_operation($$, $1, $3,'-');}

| exp STAR exp                {code_operation($$, $1, $3,'*');}

| exp DIV exp                 {code_operation($$, $1, $3,'/');}

| PO exp PF                   {$$ = $2;}


| ID                          {$$ = get_symbol_value($1->name);
                                fprintf(result_c,"  r%c%d = %s;\n", type_register($$),$$->reg_number,$$->name);
                               }

| NUMI                        {int reg = new_register();
                               $1->reg_number = reg;
                               fprintf(result_c,"  ri%d = %d;\n", reg, $1->int_val);
			       fprintf(result_header,"int ri%d;\n", reg);
                               $$ = $1;
			        }

| NUMF                        {int reg = new_register();
                               $1->reg_number = reg;
                               fprintf(result_c,"  rf%d = %f;\n", reg, $1->float_val);
			       fprintf(result_header,"float rf%d;\n", reg);
                               $$ = $1;
                               }

// II.3.1 Déréférencement

| STAR exp %prec UNA          {}

// II.3.2. Booléens

| NOT exp %prec UNA           {neg_comparison($$, $2);}
| exp INF exp                 {code_comparison($$, $1, $3, "<");}
| exp SUP exp                 {code_comparison($$, $1, $3, ">");}
| exp EQUAL exp               {code_comparison($$, $1, $3, "==");}
| exp DIFF exp                {code_comparison($$, $1, $3, "!=");}
| exp AND exp                 {code_comparison($$, $1, $3, "&&");}
| exp OR exp                  {code_comparison($$, $1, $3, "||");}

// II.3.3. Structures

| exp ARR ID                  {}
| exp DOT ID                  {}

| app                         {$$ = $1;}
;

// II.4 Applications de fonctions

app : app_id PO args PF           {$$ = tempo;}
;

app_id : ID                  {tempo = $1;} //variable tempo ajouté pour recuper l'ID de la fonction à appliquer afin de l'avoir au niveau de la regle args et arglist

;

args :  arglist               {fprintf(result_c, ");\n");}

|                             {
                               if(tempo->type_val == INT){
				 int reg = new_register();
				 fprintf(result_header,"int ri%d;\n",reg);
				 fprintf(result_c,"  ri%d = %s()",reg,tempo->name);
				 tempo->reg_number = reg;
			       }
			       else if(tempo->type_val == FLOAT){
				 int reg = new_register();
				 fprintf(result_header,"float ri%d;\n",reg);
				 fprintf(result_c,"  rf%d = %s()",reg,tempo->name);
				 tempo->reg_number = reg;
			       }
			       else if(tempo->type_val == VOID){
				 fprintf(result_c,"  %s()",tempo->name);
				 tempo->type_val = VOID;
			       }
                             }
;

arglist : exp VIR arglist     {fprintf(result_c,", r%c%d",type_register($1), $1->reg_number);}


| exp                        {
                               if(tempo->type_val == INT){
				 int reg = new_register();
				 fprintf(result_header,"int ri%d;\n",reg);
				 fprintf(result_c,"  ri%d = %s(r%c%d",reg,tempo->name,type_register($1), $1->reg_number);
				 tempo->reg_number = reg;
			       }
			       else if(tempo->type_val == FLOAT){
				 int reg = new_register();
				 fprintf(result_header,"float ri%d;\n",reg);
				 fprintf(result_c,"  rf%d = %s(r%c%d",reg,tempo->name,type_register($1), $1->reg_number);
				 tempo->reg_number = reg;
			       }
			       else if(tempo->type_val == VOID){
				 fprintf(result_c,"  %s(r%c%d",tempo->name,type_register($1), $1->reg_number);
				 tempo->type_val = VOID;
			       }
                             }
                                  				        
;


%%
int main () {

  result_c = fopen(my_files[0],"w");
  result_header = fopen(my_files[1],"w");

  fprintf(result_c, "#include <stdio.h>\n#include <stdlib.h>\n#include \"%s\"\n\n",my_files[1]);
  fprintf(result_header, "#ifndef RESULT_H\n#define RESULT_H\n\n");

  yyparse ();
  
  fprintf(result_header, "#endif");
  fclose(result_c);
  fclose(result_header);
}
