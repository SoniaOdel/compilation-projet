#include <stdio.h>
#include <stdlib.h>
#include "test.h"

int main( )
{
  ri5 = 2;
  x = ri5;
  printf("x = %d\n",x);
  ri6 = 4;
  y = ri6;
  printf("y = %d\n",y);
  ri0 = x;
  ri2 = y;
  ri7 = ri0 && ri2;
  z = ri7;
  printf("z = %d\n",z);
  ri1 = z;
  ri7 = x;
  ri8 = ri1 || ri7;
  if (!ri8)
     goto l0;
  ri9 = 3;
  a = ri9;
  printf("a = %d\n",a);
  goto l1;
l0:;
  ri7 = x;
  ri2 = y;
  ri10 = ri7 < ri2;
  a = ri10;
  printf("a = %d\n",a);

l1:;
  ri8 = z;
  ri11 = !ri8;
  b = ri11;
  printf("b = %d\n",b);
  ri12 = 0;
  return ri12;

}
